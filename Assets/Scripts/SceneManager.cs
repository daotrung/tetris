﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public GameObject[] listScene;
    public  bool isAnyCorotineRunning;
    private IEnumerator coroutine;

    public void SetActiveScene(string name)
    {
        for (int i = 0; i < listScene.Length; i++)
        {
            if (listScene[i].name == name)
            {
                listScene[i].SetActive(true);
            }
            else
            {
                listScene[i].SetActive(false);
            }
        }
    }


 
    public void PlayButton()
    {
      
            if (GameManager.instance.hasGameData())
            {


                GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
                SetActiveScene("Continues");
            }
            else
            {
                NewGameButton();
            }
        
      

    }

    public void NewGameButton()
    {
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        if(GameManager.instance.hasGameData())
        {
            GameManager.instance.ResetGameData();
        }
        coroutine = countDown(true);
        SetActiveScene("Gameplay");
        StartCoroutine(coroutine);
        GameManager.instance.tetrisBoard.enabled = true;
    }

    public void SaveAndQuitButton()
    {
        SetActiveScene("Home");
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        if (!GameManager.instance.tetrisBoard.checkNULL())
        {

            GameManager.instance.SaveGameData();

        }
            GameManager.instance.tetrisBoard.Reset();
            GameManager.instance.tetrisBoard.enabled = false;
            GameManager.instance._isPlaying = false;
        GameManager.instance.UIManager.SetBoardBackGround(false);
    }

    public void LoadGame()
    {
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        GameData data = GameManager.instance.LoadGameData();
        if(!GameManager.instance.tetrisBoard.enabled)
        {
            GameManager.instance.tetrisBoard.enabled = true;
            
        }
        SetActiveScene("Gameplay");
        coroutine = countDown(false);
        StartCoroutine(coroutine);
      
        GameManager.instance.tetrisBoard.enabled = true;
        GameManager.instance.tetrisBoard.ConvertRawDataToData(data);
        GameManager.instance.tetrisBoard.setVisible(false);

    }

    public void SettingButton()
    {
        SetActiveScene("Setting");
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);

    }

    public void PauseButton()
    {
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        if (isAnyCorotineRunning)
        {
            StopCoroutine(coroutine);
        }
        SetActiveScene("Pause");
        GameManager.instance.tetrisBoard.setVisible(false);
        GameManager.instance._isPlaying = false;
        GameManager.instance.UIManager.SetBoardBackGround(false);
    }

    public void ContinueButton()
    {
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        SetActiveScene("Gameplay");
        if (GameManager.instance.tetrisBoard.checkNULL())
        {
            ResetGame();
        }
        else
        {
            coroutine = countDown(false);
            StartCoroutine(coroutine);
        }
        


    }

    public void ResetGame()
    {
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        GameManager.instance.tetrisBoard.Reset();
        GameManager.instance.score = 0;
        GameManager.instance.UIManager.SetScore(0);
        SetActiveScene("Gameplay");
        coroutine = countDown(true);
        StartCoroutine(coroutine);
    }
    IEnumerator countDown( bool isGenerate)
    {
       
        int i = GameManager.instance.UIManager.countDownTime;
        GameManager.instance.UIManager.TxtScore.gameObject.SetActive(false);
        GameManager.instance.UIManager.SetBoardBackGround(false);
        GameManager.instance._isPlaying = false;
        isAnyCorotineRunning = true;
        while (i > 0)
        {
            GameManager.instance.UIManager.txtCountDown.fontSize = GameManager.instance.UIManager.fontCountDown;
            GameManager.instance.UIManager.txtCountDown.text = i.ToString();
            i--;
          
            yield return new WaitForSeconds(1f);
            GameManager.instance.UIManager.txtCountDown.fontSize = 0;
        }
        isAnyCorotineRunning = false;
        GameManager.instance._isPlaying = true;
        GameManager.instance.UIManager.SetBoardBackGround(true);
        GameManager.instance.tetrisBoard.setVisible(true);
        GameManager.instance.UIManager.TxtScore.gameObject.SetActive(true);
        if (isGenerate)
        {
            GameManager.instance.tetrisBoard.Generate();
        }    
        if(!isAnyCorotineRunning && !isGenerate)
        {
            GameManager.instance.tetrisBoard.showNextRandomPiece();
        }
    }
    public void Menu()
    {
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        SetActiveScene("Home");
    }
    public void GameOver()
    {
        GameManager.instance._isPlaying = false;
        GameManager.instance.UIManager.TxtGameOverHighScore.enabled = false;
        int score =     int.Parse(GameManager.instance.UIManager.TxtScore.text);
        int highScore = 0;
        if (!PlayerPrefs.HasKey("highScore"))
        {
            PlayerPrefs.SetInt("highScore",score);
        }else
        {
            highScore = PlayerPrefs.GetInt("highScore");
            GameManager.instance.UIManager.TxtGameOverHighScore.enabled = true;
            GameManager.instance.UIManager.TxtGameOverHighScore.text = highScore.ToString();
            if(score > GameManager.instance.highScore)
            {
                PlayerPrefs.SetInt("highScore", score);
            }
        }
        GameManager.instance.ResetGameData();
        GameManager.instance.UIManager.SetBoardBackGround(false);
        SetActiveScene("GameOver");
        
    }



    
}
