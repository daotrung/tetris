﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;

public class UIManager : MonoBehaviour
{



    public float fontCountDown;
    public GameObject movingButton;
    [SerializeField]private TextMeshProUGUI _txtGamePlayScore;
    [SerializeField] private TextMeshProUGUI _txtGameOverScore;
    [SerializeField] private TextMeshProUGUI _txtGameOverHighScore;
    public TextMeshProUGUI TxtScore { get => _txtGamePlayScore; }
    public TextMeshProUGUI TxtGameOverHighScore { get => _txtGameOverHighScore; set => _txtGameOverHighScore = value; }

    public TextMeshProUGUI txtCountDown;
    public int countDownTime;
    private bool _pressingLeft;
    private bool _pressingRight;
    [SerializeField]private GameObject Grid;

    #region Button 
    private void Start()
    {
        Grid.SetActive(false);
    }

    public void SetBoardBackGround(bool choice)
    {
        Grid.SetActive(choice);
    }

    public void EnableGamePad()
    {
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        GameManager.instance._isEnableControlButon = !GameManager.instance._isEnableControlButon;
        PlayerPrefs.SetString("buttonPad", GameManager.instance._isEnableControlButon.ToString());
        PlayerPrefs.Save();
       movingButton.SetActive(GameManager.instance._isEnableControlButon);

    }
    public void MoveLeftButtonDown()
    {
        Debug.Log("LDown");
         _pressingLeft = true;
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.swipeLR);
    }
    public void MoveLeftButtonUp()
    {
        Debug.Log("LUp");
        _pressingLeft = false;
       
    }

    public void MoveRightButtonDown()
    {
        Debug.Log("RDown");
        _pressingRight = true;
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.swipeLR);
    }
    public void MoveRightButtonUp()
    {
        Debug.Log("RUp");
        _pressingRight = false;
    }


    public void OnQuickDownButtonEnter()
    {
        if(!GameManager.instance.tetrisBoard.IsDecreaseFallDownTime)
        {
            GameManager.instance.tetrisBoard.DecreaseDownTime();
            
        }
    }
    public void OnQuickDownButtonUp()
    {
        GameManager.instance.tetrisBoard.ResetDropDownTime();
    }


    public void OnInstantlyDown()
    {
        GameManager.instance.tetrisBoard.IsInstanlyFallDown = true;
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.InstantlyDown);
    }

    public void Rotate()
    {
        GameManager.instance.tetrisBoard.CanRotate = true;
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.rotation);
    }
   

    #endregion
    #region audio 
    public void SetActiveAudio()
    {
      
        GameManager.instance._isEnableAudio = !GameManager.instance._isEnableAudio;
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        PlayerPrefs.SetString("sound", GameManager.instance._isEnableAudio.ToString());
        PlayerPrefs.Save();
        //change setting audio change sprite
    }

    

    public void SetActiveMusic()
    {
        GameManager.instance.audioManager.playAudio(GameManager.instance.audioManager.clickButton);
        GameManager.instance._isEnableMusic = !GameManager.instance._isEnableMusic;
        PlayerPrefs.SetString("music", GameManager.instance._isEnableMusic.ToString());
        PlayerPrefs.Save();
        if (GameManager.instance._isEnableMusic)
        {
            GameManager.instance.audioManager.playBackGroundMusic(true);
        }
        else
        {
            GameManager.instance.audioManager.playBackGroundMusic(false);
        }
        //change setting music change sprite
    }
    
    public void Rate()
    {

    }

    public void More()
    {

    }

    private void Update()
    {
     
        if (!_pressingLeft && !_pressingRight) return;
        if(_pressingLeft)
        {
            GameManager.instance.tetrisBoard.MoveLeft = true;
        }
        else
        {
            GameManager.instance.tetrisBoard.MoveRight = true;
        }
    }

    #endregion

    public void SetScore(int score)
    {
        _txtGamePlayScore.text = score.ToString();
        _txtGameOverScore.text = score.ToString();
    }
}
