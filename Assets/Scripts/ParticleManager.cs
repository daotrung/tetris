﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : ParticlePooler
{
    public void PlayEffect(Vector3 pos)
    {
        GameObject  obj = current.getPoolerParticle(pos);
        obj.SetActive(true);
    //    obj.GetComponent<ParticleSystem>().Play();
    }
}
