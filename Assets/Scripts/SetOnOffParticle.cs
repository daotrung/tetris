﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetOnOffParticle : MonoBehaviour
{
    [SerializeField]private ParticleSystem particle;
    // Start is called before the first frame update
    private void Awake()
    {
       
    }
    private void Start()
    {
        particle = GetComponent<ParticleSystem>();
    }
    private void OnEnable()
    {
        particle.Play();
    }
    // Update is called once per frame
    void Update()
    {
        if(this.gameObject.activeInHierarchy)
        {
            if(!particle.IsAlive())
            {
                this.gameObject.SetActive(false);
            }
        }
    }
}
