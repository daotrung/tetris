﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using System.Runtime.Serialization;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public SceneManager sceneManager;
    public UIManager UIManager;
    public TetrisBoard tetrisBoard;
    public AudioControls audioManager;
    public ParticleManager particleManager;
    public bool _isEnableAudio;
    public bool _isEnableMusic;
    public bool _isEnableControlButon;
    public bool _isPlaying;
    public Camera camera;
    public int score = 0;
    public int highScore = 0;

    private void Awake()
    {
        
        if(!instance)
        {
            instance = this;
        }
     
        tetrisBoard.enabled = false;
        LoadGameSetting();
    }
    
    public void LoadGameSetting()
    {
        LoadSettingMusic();
        LoadSettingSound();
        LoadControlButton();
        LoadHighScore();
        PlayerPrefs.Save();
    }

    private void LoadHighScore()
    {
        if(!PlayerPrefs.HasKey("highScore"))
        {
            PlayerPrefs.SetInt("highScore", highScore);
        }
        else
        {
           
            highScore = PlayerPrefs.GetInt("highScore");
        }
    }

    private void LoadControlButton()
    {
        if (!PlayerPrefs.HasKey("buttonPad"))
        {
            PlayerPrefs.SetString("buttonPad", _isEnableControlButon.ToString());
        }
        else
        {
            _isEnableControlButon = Boolean.Parse(PlayerPrefs.GetString("buttonPad"));
        }
    }

    private void LoadSettingSound()
    {
        if (!PlayerPrefs.HasKey("sound"))
        {
            PlayerPrefs.SetString("sound", _isEnableAudio.ToString());
        }
        else
        {
            _isEnableAudio = Boolean.Parse(PlayerPrefs.GetString("sound"));
        }
    }

    private void LoadSettingMusic()
    {
        if (!PlayerPrefs.HasKey("music"))
        {
            PlayerPrefs.SetString("music", _isEnableMusic.ToString());
        }
        else
        {
            _isEnableMusic = Boolean.Parse(PlayerPrefs.GetString("music"));
        }
    }

    public bool LoadButtonCustomSetting(string key)
    {
        return bool.Parse(PlayerPrefs.GetString(key));
        
    }
    //serialization
    public GameData LoadGameData()
    {
        try
        {
            string destination = Application.persistentDataPath + "/save.dat";
            FileStream file;
            if (File.Exists(destination))
            {
                file = File.OpenRead(destination);

            }
            else
            {
                UnityEngine.Debug.LogError("File not found");
                return null;
            }
            BinaryFormatter bf = new BinaryFormatter();
            GameData data = (GameData)bf.Deserialize(file);
            file.Close();
            UnityEngine.Debug.Log("Load File successful");
            return data;
        }catch(SerializationException ex)
        {
            UnityEngine.Debug.Log("Load Failure,Reset data ");
            ResetGameData();
            return null;
        }
    }
    public bool hasGameData()
    {
        try
        {
            string destination = Application.persistentDataPath + "/save.dat";
            if (File.Exists(destination))
            {
                FileStream file = File.OpenRead(destination);
                BinaryFormatter bf = new BinaryFormatter();
                GameData data = (GameData)bf.Deserialize(file);
                file.Close();
                return true;
            }
        }catch(SerializationException ex)
        {
            UnityEngine.Debug.Log("Load Failure,Reset data ");
            return false;
        }catch(IOException e)
        {
            UnityEngine.Debug.Log("Load Failure,Reset data ");
            return false;
        }
        return false;
    }
    public bool SaveGameData()
    {
        try
        {
            string destination = Application.persistentDataPath + "/save.dat";
            FileStream file;
            if (File.Exists(destination)) file = File.OpenWrite(destination);
            else file = File.Create(destination);

            BlockRaw[,] board = tetrisBoard.ConvertBoardToRawData(); //doi board bang cac object da doi khoi tao
            BlockRaw[] piece = tetrisBoard.ConvertPieceToRawData();
            int[] infoRd = tetrisBoard.CovertInfoRandomPieceToRawData();
            GameData data = new GameData(piece, board, score,infoRd[0],infoRd[1]);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, data);
            file.Close();
            UnityEngine.Debug.Log("Save File Successful");
            return true;
        }catch(Exception ex)
        {
            UnityEngine.Debug.Log("Save File Failure");
            ResetGameData();
            return false;
        }
    }
    public void ResetGameData()
    {
        string destionation = Application.persistentDataPath + "/save.dat";
      
        try
        {
            if (File.Exists(destionation))
            {
                System.IO.File.Delete(destionation);
             //   File.Delete(destionation);
                UnityEngine.Debug.Log("Data reset Complete!");
            }
            else
            {
                UnityEngine.Debug.Log("No save data to delete");
            }
        }catch(IOException e)
        {
            Debug.Log(e.Message);
            //can fix loi IOException
           

        }
    }
    
    
    
    void Start()
    {
        LoadHighScore();
        UIManager.movingButton.SetActive(_isEnableControlButon);
        sceneManager = GetComponent<SceneManager>();
        sceneManager.SetActiveScene("Home");
        if(_isEnableMusic)
        {
            //play music
            audioManager.playBackGroundMusic();
        }
    }

}
