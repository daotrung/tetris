﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
[System.Serializable]
public class GameData
{
    public int score;
    public BlockRaw[] pieceRaw;
    public BlockRaw[,] boardRaw ;
    public int rdNextType;
    public int rdNextColor;
    public GameData()
    {
        pieceRaw = new BlockRaw[4];
        boardRaw = new BlockRaw[10, 20];
    }
    public GameData(BlockRaw[] piece,BlockRaw[,] board,int score)
    {
        this.score = score;
        pieceRaw = piece;
        boardRaw = board;
       
    }
    public GameData(BlockRaw[] piece, BlockRaw[,] board, int score,int type,int color)
    {
        this.score = score;
        pieceRaw = piece;
        boardRaw = board;
        this.rdNextType = type;
        this.rdNextColor = color;
    }


}
