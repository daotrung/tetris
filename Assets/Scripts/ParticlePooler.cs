﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePooler : MonoBehaviour
{
    public static ParticlePooler current; 
    [SerializeField]private GameObject poolerParticle;
    [SerializeField] List<GameObject> pooledList;
    public int amount;
    public bool WillGrow;
    // Start is called before the first frame update
    private  void Awake()
    {

        if (current == null)
        {
            current = this;
        }
    }
    void Start()
    {
        pooledList = new List<GameObject>();
        for(int i = 0; i < amount;i++)
        {
            GameObject particle = Instantiate(poolerParticle);
            particle.SetActive(false);
            pooledList.Add(particle);
        }
    }
    public GameObject getPoolerParticle()
    {
        for(int i = 0; i < pooledList.Count;i++)
        {
            if(!pooledList[i].activeInHierarchy)
            {
                return pooledList[i];
            }
        }
        if(WillGrow)
        {
            GameObject particle = Instantiate(poolerParticle);
            pooledList.Add(particle);
            particle.SetActive(false);
            return particle;
        }
        return null;
    }
    public GameObject getPoolerParticle(string tag)
    {
        for (int i = 0; i < pooledList.Count; i++)
        {
            if (!pooledList[i].activeInHierarchy )
            {
                if (pooledList[i].tag == tag)
                {
                    return pooledList[i];
                }
            }
        }
        if (WillGrow)
        {
            GameObject particle = Instantiate(poolerParticle);
            pooledList.Add(particle);
            particle.SetActive(false);
            return particle;
        }
        return null;
    }

    public GameObject getPoolerParticle(Vector3 pos)
    {
        for (int i = 0; i < pooledList.Count; i++)
        {
            if (!pooledList[i].activeInHierarchy)
            {
                pooledList[i].transform.localPosition = pos;
                    return pooledList[i];
             
            }
        }
        if (WillGrow)
        {
            GameObject particle = Instantiate(poolerParticle);
            particle.transform.localPosition = pos;
            particle.SetActive(false);
            pooledList.Add(particle);
            return particle;
        }
        return null;
    }
    public GameObject getPoolerParticle(Vector3 pos,string tag)
    {
        for (int i = 0; i < pooledList.Count; i++)
        {
            if (!pooledList[i].activeInHierarchy)
            {
                if (pooledList[i].tag == tag)
                {
                    pooledList[i].transform.localPosition = pos;
                    return pooledList[i];
                }
            }
        }
        if (WillGrow)
        {
            GameObject particle = Instantiate(poolerParticle);
            particle.transform.localPosition = pos;
            particle.SetActive(false);
            pooledList.Add(particle);
            return particle;
        }
        return null;
    }

}
