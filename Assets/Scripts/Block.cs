﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct Block 
{
    public int x;
    public int y;
    public GameObject ob;
   

    public Block(int x, int y, GameObject ob)
    {
        this.x = x;
        this.y = y;
        this.ob = ob;
     
    }
    public override string ToString()
    {
        return "x: " + x + ", y: "+y + ", ob: " + ob.name;
    }
}
