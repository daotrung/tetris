﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;


public class TetrisBoard : ObjectPooler
{
    #region variables
    [Header("Size of board")]
    public static int width = 10;
    public static int height = 20;
    [Header("Prefabs")]
    public GameObject blockPrefabs;
    public Sprite[] blockSprite;
    public Material[] materialTrailSprite;
    private Block[,] _board;
    private Block temp;
    private Block[] _piece = new Block[4];
    private Block[] _rdNextPiece = new Block[4];
    [Header("Time")]
    [SerializeField] private float _moveTimeD = 0;
    [SerializeField] private float _moveSpeedD;
    [SerializeField] private float _moveTimeLR = 0;
    [SerializeField] private float _moveSpeedLR = 0.06f;
    [SerializeField] [Range(0, 1)] float _tapTime = 0.3f;

    [SerializeField] [Range(0, 1f)] float timeToFlipDown;
    private float _startTime = 0f;
    private float _endTime = 0f;
    private int[,] figures = new int[,] { { 1, 3, 5, 7 }, //I
        { 2, 4, 5, 7 }, //Z
        { 3, 4, 5, 6 }, //S 
        { 3, 5, 4, 7 }, //T 
        { 2, 5, 3, 7 }, //L 
        { 3, 5, 6, 7 }, //J
        { 2, 3, 4, 5 } //[]
    };

    //variable for roatation
    [SerializeField] private int _shiftAfterXAxis = 1;
    [SerializeField] private int _shiftBeforeXAxis = 0;
    [SerializeField] private bool _isSpecialBlock = false;
    //variable for Touch 
    [SerializeField] private float _DistanceDropDownInstanly = 4;
    [SerializeField] private float _timesToReduceFallDownTime = 20;

    [SerializeField] [Range(0, 20)] private int touchSensitivityHorizontal;
    [SerializeField] [Range(0, 20)] private int touchSensitivityVertical;
    private Vector2 _startTouch;
    private Vector2 _endTouch;
    Vector2 previousUnitPosition = Vector2.zero;
    Vector2 direction = Vector2.zero;

    private bool _isDecreaseFallDownTime;
    private bool _CancelDownTime;
    //variable for movemet
    private bool _moveLeft;
    private bool _moveRight;
    private bool _canRotate;
    private bool _isInstanlyFallDown;
    //variable for random block;
    private int _rdNextTypeBlock = -1;
    private int _rdNextColor = -1;
    [Header("Block")]
    //variable for nexrt random block 
    public Vector3 scaleForBlocks;
    public Vector3 offSetPos;
    public Vector3 scaleOfRdNewBlock;
    public GameObject boundingObject;

    //variable for coroutine
    private IEnumerator coroutine;
    //property
    public bool MoveLeft { get => _moveLeft; set => _moveLeft = value; }
    public bool MoveRight { get => _moveRight; set => _moveRight = value; }
    public bool CanRotate { get => _canRotate; set => _canRotate = value; }

    public bool IsDecreaseFallDownTime { get => _isDecreaseFallDownTime; set => _isDecreaseFallDownTime = value; }
    public bool IsInstanlyFallDown { get => _isInstanlyFallDown; set => _isInstanlyFallDown = value; }
    public Block[] Piece { get => _piece; }
    public Block[,] Board { get => _board; }

    int[] point = { 5, 10, 25, 50 };

    #endregion
    protected override void Start()
    {
        base.Start();
        if (_board == null)
        {
            _board = new Block[width, height];

        }
        _moveSpeedD = Time.deltaTime * 30;
        boundingObject.transform.localScale = scaleOfRdNewBlock;
    }
    // Update is called once per frame
    private void Update()
    {
        if (GameManager.instance._isPlaying)
        {


            UpdateStandalone();
            if (!GameManager.instance._isEnableControlButon)
            {
                UpdateMobile();
            }
            movingProcess();

            MoveDown(0, -1);

        }
    }
    #region handle input
    private void movingProcess()
    {

        if (_moveLeft)
        {

            MoveLR(-1, 0);
        }
        else if (_moveRight)
        {

            MoveLR(1, 0);
        }
        if (_canRotate)
        {
            Rotate();
        }
        if (_isInstanlyFallDown)
        {
            InstanlyFallDown();
        }
        ResetMoving();

    }
    #endregion
    #region mobile control

    private void UpdateMobile()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touchCount == 1)
            {
                Touch touch = Input.GetTouch(0);

                //swipe horizontal
                if (touch.phase == TouchPhase.Began)
                {
                    previousUnitPosition = new Vector2(touch.position.x, touch.position.y);
                    _startTouch = Camera.main.ScreenToWorldPoint(touch.position);
                    _startTime = Time.time;

                }

                else if (touch.phase == TouchPhase.Moved)
                {
                    Vector2 touchdeltaPosition = touch.deltaPosition;
                    direction = touchdeltaPosition.normalized;
                    if (Mathf.Abs(touch.position.x - previousUnitPosition.x) >= touchSensitivityHorizontal && direction.x < 0 && touch.deltaPosition.y > -10 && touch.deltaPosition.y < 10)
                    {
                        _moveLeft = true;

                    }
                    else if (Mathf.Abs(touch.position.x - previousUnitPosition.x) >= touchSensitivityHorizontal && direction.x > 0 && touch.deltaPosition.x > -10 && touch.deltaPosition.y < 10)
                    {
                        _moveRight = true;

                    }
                    else if (Math.Abs(touch.position.x - previousUnitPosition.x) >= touchSensitivityVertical && direction.y < 0 && touch.deltaPosition.x > -10 && touch.deltaPosition.x < 10 && !_isDecreaseFallDownTime)
                    {
                        DecreaseDownTime();
                    }

                }
                //tap
                if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                {

                    _endTouch = Camera.main.ScreenToWorldPoint(touch.position);
                    _endTime = Time.time;
                    float dSwipe = _startTouch.y - _endTouch.y;
                    if (Mathf.Abs(dSwipe) >= _DistanceDropDownInstanly)
                    {
                        _isInstanlyFallDown = true;
                        Debug.Log("Max Swipe");
                    }
                    if (_endTime - _startTime <= _tapTime)
                    {
                        if (Vector3.Distance(_startTouch, _endTouch) <= 0.5f)
                        {

                            _canRotate = true;
                        }
                    }
                    ResetDropDownTime();
                    ResetTouchPos();
                    _CancelDownTime = true;

                }



            }
        }
    }

    #endregion
    #region pc control
    private void UpdateStandalone()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            TurnOffEffectBlocks();
            Move(-1, 0);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            TurnOffEffectBlocks();
            Move(1, 0);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            _canRotate = true;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (!IsDecreaseFallDownTime)
            {
                _moveSpeedD /= 10;
            }

        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            ResetDropDownTime();
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            _isInstanlyFallDown = true;
        }

    }
    #endregion
    #region movement
    private void InstanlyFallDown()
    {
        for (int i = 0; ; i++)
        {
            if (!Move(0, -1))
            {
                break;
            }
        }
    }
    private void MoveLR(int dx, int dy)
    {
        _moveTimeLR += Time.deltaTime;
        if (_moveTimeLR >= _moveSpeedLR)
        {
            TurnOffEffectBlocks();
            if (!Move(dx, dy))
            {
                Vector3 dir = Vector3.zero;
                if (dx == 1)
                {
                    dir.x = 1;
                }
                else
                {
                    dir.x = -1;
                }
                GameManager.instance.camera.GetComponent<CameraShakeByDirection>().shake(dir);
            }
            _moveTimeLR = 0;
        }
    }
    //move down
    private void MoveDown(int dx, int dy)
    {

        _moveTimeD += Time.deltaTime;
        if (_moveTimeD >= _moveSpeedD)
        {
            if (!Move(0, -1))
            {
                TurnOffEffectBlocks();
                for (int i = 0; i < 4; i++)
                {
                    _board[_piece[i].x, -_piece[i].y] = _piece[i];
                    _board[_piece[i].x, -_piece[i].y].ob.name = "board" + _board[_piece[i].x, -_piece[i].y].x + "_" + _board[_piece[i].x, -_piece[i].y].y;
                }

                Generate();
                Clear();
            }

            _moveTimeD = 0;
            if (IsDecreaseFallDownTime && _CancelDownTime)
            {
                ResetDropDownTime();
            }

        }

    }
    //move
    private bool Move(int dx, int dy)
    {

        Block[] origin = _piece.Clone() as Block[];
        for (int i = 0; i < 4; i++)
        {
            _piece[i].x += dx;
            _piece[i].y += dy;

        }
        return CheckAndSet(origin);
    }

    private bool CheckAndSet(Block[] origin)
    {
        TurnOnEffectBlocks();
        bool set = true;
        for (int i = 0; i < 4; i++)
        {

            if (_piece[i].x < 0 || _piece[i].x >= width || _piece[i].y <= -height)
            {
                set = false;
            }
            else if (_board[_piece[i].x, -_piece[i].y].ob != null && _board[_piece[i].x, -_piece[i].y].ob.activeInHierarchy)
            {
                set = false;
            }
        }

        if (set)
        {
            for (int i = 0; i < 4; i++)
            {
                _piece[i].ob.transform.position = new Vector2(_piece[i].x, _piece[i].y);
            }
        }
        else
        {
            _piece = origin;
        }
        return set;
    }

    private bool CheckAndSet(Block[] origin, bool isRotate)
    {
        TurnOffEffectBlocks();
        bool set = true;

        bool useWallKickLeft = false;
        int count = 0;
        bool useWallKickRight = false;
        for (int i = 0; i < 4; i++)
        {
            if (_piece[i].x < 0 && _piece[i].y > -height)
            {
                useWallKickLeft = true;
                count++;
            }
            else if (_piece[i].x >= width && _piece[i].y > -height)
            {
                useWallKickRight = true;
                count--;
            }

            if (_piece[i].y <= -height)
            {
                set = false;
            }

        }

        if (useWallKickLeft || useWallKickRight)
        {
            Move(count, 0);
        }
        if (set)
        {
            for (int i = 0; i < 4; i++)
            {

                _piece[i].ob.transform.position = new Vector2(_piece[i].x, _piece[i].y);

            }
        }
        else
        {
            _piece = origin;
        }

        return set;
    }

    private bool Rotate()
    {
        // fix pivot point for [], and T like tetris grand master
        if (_isSpecialBlock) return false;
        Block[] origin = _piece.Clone() as Block[];
        Block pivot = _piece[1];
        for (int i = 0; i < 4; i++)
        {

            int newX = pivot.x + pivot.y - _piece[i].y;
            int newY = pivot.y + _piece[i].x - pivot.x;
            _piece[i].x = newX;
            _piece[i].y = newY;

        }
        return CheckAndSet(origin, true);
    }
    #endregion
    #region utility
    public void Generate()
    {
        int rdType = _rdNextTypeBlock;
        int rdColor = _rdNextColor;
        if ((rdType == -1) && (rdColor == -1))
        {

            rdType = UnityEngine.Random.Range(0, figures.GetLength(0));
            rdColor = UnityEngine.Random.Range(0, blockSprite.Length);
        }
        checkSpecialBlock(rdType);
        for (int i = 0; i < 4; i++)
        {
            _piece[i].x = figures[rdType, i] % 2;
            _piece[i].y = -figures[rdType, i] / 2;
        }


        Sprite sprite = blockSprite[rdColor];
        for (int i = 0; i < 4; i++)
        {
            Vector2 pos = new Vector2(_piece[i].x, _piece[i].y);
            _piece[i].ob = current.GetPooledObject(pos, sprite);
            _piece[i].ob.GetComponent<TrailRenderer>().material = materialTrailSprite[rdColor];
            _piece[i].ob.SetActive(true);
            _piece[i].ob.name = "piece: " + i;

        }

        if (!Move(4, 0))
        {
            //gameOver
            Reset();
            GameManager.instance.sceneManager.GameOver();

        }
        _rdNextTypeBlock = UnityEngine.Random.Range(0, figures.GetLength(0));
        _rdNextColor = UnityEngine.Random.Range(0, blockSprite.Length);
        showNextRandomPiece();

    }
    public void showNextRandomPiece()
    {
        if (_rdNextTypeBlock == -1 ||_rdNextColor == -1) return;
        for (int i = 0; i < 4; i++)
        {
            _rdNextPiece[i].x = figures[_rdNextTypeBlock, i] % 2;
            _rdNextPiece[i].y = -figures[_rdNextTypeBlock, i] / 2;
        }


        Sprite sprite = blockSprite[_rdNextColor];
        if (_rdNextPiece[0].ob == null)
        {

            for (int i = 0; i < 4; i++)
            {
                Vector3 pos = new Vector3(_rdNextPiece[i].x, _rdNextPiece[i].y,0);
                _rdNextPiece[i].ob = current.GetPooledObject(pos, sprite);
                _rdNextPiece[i].ob.SetActive(true);
               
                _rdNextPiece[i].ob.transform.SetParent(boundingObject.transform);
                _rdNextPiece[i].ob.transform.localPosition = pos+offSetPos ;
                _rdNextPiece[i].ob.transform.localScale = scaleForBlocks;
                _trailRef = _rdNextPiece[i].ob.GetComponent<TrailRenderer>();
                _trailRef.enabled = false;
                _rdNextPiece[i].ob.name = "randomBlock_"+i;
           }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                Vector3 pos = new Vector3(_rdNextPiece[i].x, _rdNextPiece[i].y);
                _rdNextPiece[i].ob.GetComponent<SpriteRenderer>().sprite = sprite;
                
                _rdNextPiece[i].ob.transform.localPosition = pos+offSetPos;
            }
        }
    }
    private void checkSpecialBlock(int type)
    {
        if (type == (figures.GetLength(0) - 1))
        {
            _isSpecialBlock = true;
        }
        else
        {
            _isSpecialBlock = false;
        }
    }
    public void DecreaseDownTime()
    {
        _moveSpeedD /= _timesToReduceFallDownTime;
        IsDecreaseFallDownTime = !IsDecreaseFallDownTime;
    }
    private void Clear()
    {
        List<Block> listClearBlocks = new List<Block>();
        int k = height - 1;
        int dy = 0;
        for (int i = height - 1; i > 0; i--)
        {
            listClearBlocks.Clear();
            int count = 0;
            for (int j = 0; j < width; j++)
            {
                if (_board[j, i].ob != null)
                {
                    count++;
                    listClearBlocks.Add(_board[j, i]);
                }
                _board[j, i].y += dy;

                _board[j, k] = _board[j, i];
            }
            if (count < width)
            {
                k--;
            }
            else
            {
                dy += -1;
                for (int x = 0; x < listClearBlocks.Count; x++)
                {
                    coroutine = Fade(listClearBlocks[x].ob);
                    StartCoroutine(coroutine);
                }
            }
            //set new pos
            for (int j = 0; j < width; j++)
            {
                if (_board[j, i].ob != null)
                {
                    //_board[j, i].ob.transform.position = new Vector2(_board[j, i].x, _board[j, i].y);
                    coroutine = SlipDown(_board[j, i].ob, new Vector3(_board[j, i].x, _board[j, i].y, 0));
                    StartCoroutine(coroutine);
                }
            }
        }
        int score = Mathf.Abs(dy);
        if (score > 0)
        {
            GameManager.instance.score = GameManager.instance.score + (score >= 4 ? score * point[3] : score * point[score - 1]);
            GameManager.instance.UIManager.SetScore(GameManager.instance.score);
        }
    }
    private void TurnOffEffectBlocks()
    {
        if (_trailRef == null) return;
        _trailRef = _piece[0].ob.GetComponent<TrailRenderer>();
        if (_trailRef.enabled == false) return;
        for (int i = 0; i < 4; i++)
        {
            _trailRef = _piece[i].ob.GetComponent<TrailRenderer>();
            _trailRef.Clear();
            _trailRef.enabled = false;
        }
    }
    private void TurnOnEffectBlocks()
    {
        if (_trailRef == null) return;
        _trailRef = _piece[0].ob.GetComponent<TrailRenderer>();
        if (_trailRef.enabled == true) return;
        for (int i = 0; i < 4; i++)
        {
            _trailRef = _piece[i].ob.GetComponent<TrailRenderer>();
            _trailRef.enabled = true;
        }
    }
    public void ResetDropDownTime()
    {
        IsDecreaseFallDownTime = false;
        _moveSpeedD = Time.deltaTime * 30;
    }
    private void ResetTouchPos()
    {
        _endTouch = Vector2.zero;
        _startTouch = Vector2.zero;
        previousUnitPosition = Vector2.zero;
        direction = Vector2.zero;
    }
    private void ResetMoving()
    {

        _moveLeft = false;
        _moveRight = false;
        _canRotate = false;
        _isInstanlyFallDown = false;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            previousUnitPosition = touch.position;
        }
    }
    private void ResetRandomNextBlock()
    {
        if (_rdNextPiece[3].ob == null) return;
        _rdNextColor = -1;
        _rdNextTypeBlock = -1;
        for(int i = 0; i < 4;i++)
        {
            _rdNextPiece[i].ob.transform.parent = null;
            _rdNextPiece[i].ob.SetActive(false);
            _rdNextPiece[i].ob = null;
        }
    }
    public void Reset()
    {
        if (_board != null)
        {
            Debug.Log("Reset");
            ResetRandomNextBlock();
            ResetDropDownTime();
            ResetMoving();
            ResetTrail();
            ResetTouch();
            _isSpecialBlock = false;
            _CancelDownTime = false;
            _moveTimeD = 0;

            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {

                    if (_board[i, j].ob != null)
                    {
                        _board[i, j].ob.SetActive(false);
                        _board[i, j].ob = null;
                    }


                }
            }
            for (int i = 0; i < 4; i++)
            {
                if (_piece[i].ob)
                {
                    _piece[i].ob.SetActive(false);
                }
                _piece[i].ob = null;
            }
        }
    }
    private void ResetTrail()
    {
        _trailRef = null;
    }
    private void ResetTouch()
    {
        _startTouch = Vector2.zero;
        _endTouch = Vector2.zero;
    }
    public bool checkNULL()
    {
        if (_piece[3].ob == null) return true;
        return false;
    }
    public Sprite getSpriteByName(string name)
    {
        for (int i = 0; i < blockSprite.Length; i++)
        {
            if (blockSprite[i].name == name)
            {
                return blockSprite[i];
            }
        }
        return null;
    }
    public void setVisible(bool choice)
    {
        if (_piece != null)
        {
            for (int i = 0; i < 4; i++)
            {
                if (!_piece[i].ob)
                {

                    break;
                }
                _piece[i].ob.SetActive(choice);
            }
        }
        if (_board != null)
        {
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    if (_board[i, j].ob != null)
                    {
                        _board[i, j].ob.SetActive(choice);
                    }
                }
            }
        }
    }
    #endregion
    #region load data
    public void ConvertRawDataToData(GameData data)
    {
        if (_board == null)
        {
            _board = new Block[width, height];
        }
        Debug.Log("Convert Raw Data");
        GameManager.instance.score = data.score;
        GameManager.instance.UIManager.SetScore(GameManager.instance.score);
        Sprite sprite = getSpriteByName(data.pieceRaw[0].nameSprite);
        //convert from pieceRaw  to piece
        if (data != null)
        {
            for (int i = 0; i < 4; i++)
            {
                _piece[i].x = data.pieceRaw[i].x;
                _piece[i].y = data.pieceRaw[i].y;
                Vector2 pos = new Vector2((int)data.pieceRaw[i].position[0], (int)data.pieceRaw[i].position[1]);
                _piece[i].ob = current.GetPooledObject(pos, sprite);
                _piece[i].ob.SetActive(true);
                _piece[i].ob.name = "piece: " + i;
            }

            //convert form boardRaw to board
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (!data.boardRaw[i, j].isNull)
                    {
                        sprite = getSpriteByName(data.boardRaw[i, j].nameSprite);
                        Vector2 pos = new Vector2((int)data.boardRaw[i, j].position[0], (int)data.boardRaw[i, j].position[1]);
                        _board[i, j].x = data.boardRaw[i, j].x;
                        _board[i, j].y = data.boardRaw[i, j].y;
                        _board[i, j].ob = current.GetPooledObject(pos, sprite);
                        _board[i, j].ob.SetActive(true);
                        _board[i, j].ob.name = "board_x: " + i + ",y : " + j;
                    }

                }
            }
            _rdNextTypeBlock = data.rdNextType;
            _rdNextColor = data.rdNextColor;
           
        }


    }
    public BlockRaw[,] ConvertBoardToRawData()
    {
        BlockRaw[,] rawData = new BlockRaw[width, height];

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                rawData[i, j] = new BlockRaw();
                if (_board[i, j].ob == null)
                {
                    rawData[i, j].isNull = true;
                }
                else
                {
                    rawData[i, j].x = _board[i, j].x;
                    rawData[i, j].y = _board[i, j].y;
                    rawData[i, j].position[0] = _board[i, j].ob.transform.position.x;
                    rawData[i, j].position[1] = _board[i, j].ob.transform.position.y;
                    rawData[i, j].position[2] = _board[i, j].ob.transform.position.z;
                    rawData[i, j].nameSprite = _board[i, j].ob.GetComponent<SpriteRenderer>().sprite.name;
                    rawData[i, j].isNull = false;
                }
            }
        }
        return rawData;
    }
    public BlockRaw[] ConvertPieceToRawData()
    {



        BlockRaw[] rawData = null;


        rawData = new BlockRaw[4];
        string nameSprite = _piece[0].ob.GetComponent<SpriteRenderer>().sprite.name;
        for (int i = 0; i < 4; i++)
        {
            rawData[i] = new BlockRaw();
            rawData[i].x = _piece[i].x;
            rawData[i].y = _piece[i].y;
            rawData[i].position[0] = _piece[i].ob.transform.position.x;
            rawData[i].position[1] = _piece[i].ob.transform.position.y;
            rawData[i].position[2] = _piece[i].ob.transform.position.z;
            rawData[i].nameSprite = nameSprite;
            rawData[i].isNull = false;
        }

        return rawData;
    }
    public int[] CovertInfoRandomPieceToRawData()
    {
        int[] info = { _rdNextTypeBlock, _rdNextColor };
        return info;
    }

    #endregion
    #region effect
    IEnumerator  Fade(GameObject obj)
    {
        bool isRunning = true;
        float intensity = 1;
        SpriteRenderer sprite = obj.GetComponent<SpriteRenderer>();
        while(isRunning)
        {
            intensity -= Time.deltaTime;
            sprite.color = new Color(sprite.color.r, sprite.color.b, sprite.color.g, intensity);
          
            yield return new WaitForEndOfFrame();
            if (intensity <= 0)
            {
                isRunning = false;
            }
        }
        
        obj.SetActive(false);
        yield return new WaitForEndOfFrame();
    }
    IEnumerator SlipDown(GameObject obj,Vector3 target)
    {
        //float timeTakeDuringLerp  = 1;
        //float timeStartLerp = Time.time;
        //float timeSinceStart = Time.time - timeStartLerp;
        //float percentageComplete = timeSinceStart / timeTakeDuringLerp;
        //yield return new WaitForSeconds(.1f);
        //while(percentageComplete >= 1.0f)
        //{ 
        //    obj.transform.position = Vector3.Slerp(obj.transform.position, target, percentageComplete);
        //    timeSinceStart = Time.time - timeStartLerp;
        //    percentageComplete = timeSinceStart / timeTakeDuringLerp;
        //    yield return new WaitForEndOfFrame();
        //}
        //yield return null;
        yield return new WaitForSeconds(timeToFlipDown);
        obj.transform.position = target;
    }

    #endregion
}

