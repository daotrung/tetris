﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blockSetting : MonoBehaviour
{
    [SerializeField]private SpriteRenderer _sr;
    [SerializeField]private TrailRenderer _tr;
   

    public SpriteRenderer Sr { get => _sr; set => _sr = value; }
    public TrailRenderer Tr { get => _tr; set => _tr = value; }
  

    // Start is called before the first frame update
    void Start()
    {
        _sr = this.gameObject.GetComponent<SpriteRenderer>();
        _tr = this.gameObject.GetComponent<TrailRenderer>();
     
    }

  
}
