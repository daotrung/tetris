﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SetOnOffSpriteButton : MonoBehaviour
{
    public Sprite on;
    public Sprite off;
    private Button _button;
    public bool setting;
    private string tagButton;
    int count = 0;
    
    void Start()
    {
        _button = GetComponent<Button>();
        tagButton = this.gameObject.tag;
        OnChangeSpriteButton();
    }

   
    public void OnChangeSpriteButton()
    {
        setting = GameManager.instance.LoadButtonCustomSetting(tagButton);
        if(setting)
        {
            _button.image.overrideSprite = on;
        }
        else
        {
            _button.image.overrideSprite = off;
        }
       
       
    }

    
}
