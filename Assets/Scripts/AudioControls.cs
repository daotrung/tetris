﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControls : MonoBehaviour
{
    private AudioSource _audioMusic;
    private AudioSource _audioSound;
   
    public AudioClip musicBackGround;
    public AudioClip musicGamePlayBackGround;
    public AudioClip clickButton;
    public AudioClip swipeDown;
    public AudioClip swipeInstantlyFallDown;
    public AudioClip swipeLR;
    public AudioClip movingBlock;
    public AudioClip setBlock;
    public AudioClip InstantlyDown;
    public AudioClip rotation;
    // audio for block move
    //audio for set pos when can't move

    private void Awake()
    {
        _audioMusic = this.gameObject.AddComponent<AudioSource>();
        _audioMusic.clip = musicBackGround;
        _audioMusic.loop = true;
        _audioMusic.playOnAwake = false;

        _audioSound = this.gameObject.AddComponent<AudioSource>();
        _audioSound.playOnAwake = false;
        

      
    }
    public void playBackGroundMusic()
    {
        _audioMusic.Play();
    }

    public void playBackGroundMusic(bool canPlay)
    {
        if(!canPlay)
        {
            if (_audioMusic.isPlaying)
            {
                _audioMusic.Pause();
            }
        }
        else
        {
            if (!_audioMusic.isPlaying && _audioMusic.time != 0)
            {
                _audioMusic.UnPause();
            }else
            {
                _audioMusic.Play();
            }
        }
    }
    

    public void playAudio(AudioClip clip)
    {
        if (GameManager.instance._isEnableAudio)
        {
            _audioSound.clip = clip;
            _audioSound.PlayOneShot(clip);
        }
    }

   
}
