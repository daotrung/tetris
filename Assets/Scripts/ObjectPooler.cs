﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler current;
    [SerializeField] GameObject pooledObject;

    [SerializeField]private List<GameObject> pooledObjects;
    
    public int pooledAmount;
    public bool willGrow ;
    protected TrailRenderer _trailRef;
    public Vector3 NorimalizeScale;
    // Start is called before the first frame update
    private void Awake()
    {
       if(current == null)
        {
            current = this;
        }
    }
    protected virtual void Start()
    {
        NorimalizeScale = new Vector3(1, 1, 1);
        pooledObjects = new List<GameObject>();
        for(int i = 0; i < pooledAmount;i++)
        {
            GameObject obj =(GameObject)Instantiate(pooledObject);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
      
    }

    // Update is called once per frame
    public GameObject GetPooledObject()
    {
        for(int i = 0; i < pooledObjects.Count;i++)
        {
            if(!pooledObjects[i].activeInHierarchy)
            {
                pooledObjects[i].transform.localScale = NorimalizeScale;
                SpriteRenderer rendener = pooledObjects[i].GetComponent<SpriteRenderer>();
                rendener.color = new Color(rendener.color.r,rendener.color.b,rendener.color.g,1);
                pooledObjects[i].GetComponent<SpriteRenderer>().enabled = true;
                _trailRef = pooledObjects[i].GetComponent<TrailRenderer>();
                _trailRef.enabled = true;
                return pooledObjects[i];
            }
        }
        if(willGrow)
        {
            GameObject obj = (GameObject)Instantiate(pooledObject);
            pooledObjects.Add(obj);
            obj.SetActive(false);
            return obj;
        }
        return null;
    }
    //object pooling for 2d gameobject
    #region 2D
    public GameObject GetPooledObject(Vector2 pos,Sprite sprite)
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].activeInHierarchy)
            {
                pooledObjects[i].transform.position = pos;
                pooledObjects[i].transform.localScale = NorimalizeScale;
                SpriteRenderer spriteRender = pooledObjects[i].GetComponent<SpriteRenderer>();
                spriteRender.sprite = sprite;
                spriteRender.color = new Color(spriteRender.color.r, spriteRender.color.b, spriteRender.color.g, 1);
                _trailRef = pooledObjects[i].GetComponent<TrailRenderer>();
                _trailRef.enabled = true;
                return pooledObjects[i];
            }
        }
        if (willGrow)
        {
            GameObject obj = (GameObject)Instantiate(pooledObject,pos,Quaternion.identity);
            SpriteRenderer spriteRender = obj.GetComponent<SpriteRenderer>();
            spriteRender.sprite = sprite;
            pooledObjects.Add(obj);
            obj.SetActive(false);
            return obj;
        }
        return null;
    }
    //object pooling for Particle object

    #endregion

}
