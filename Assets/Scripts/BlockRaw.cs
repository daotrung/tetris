﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class BlockRaw 
{
    public int x;
    public int y;
    public float[] position = new float[3];
    public string nameSprite;
    public bool isNull;
    public BlockRaw()
    {
       
    }
    public BlockRaw(Block block)
    {
        SetValue(block);
       
    }

    public BlockRaw(BlockRaw block)
    {
        this.x = block.x;
        this.y = block.y;
        this.position[0] = block.position[0];
        this.position[1] =block.position[1];
        this.position[2] = block.position[2];
        this.nameSprite = block.nameSprite;
        this.isNull = block.isNull;
    }
    public void SetValue(Block block)
    {
        if (!block.ob)
        {
            this.x = block.x;
            this.y = block.y;
            isNull = true;
        }
        else
        {
            this.x = block.x;
            this.y = block.y;
            position = new float[3];
            position[0] = block.ob.transform.position.x;
            position[1] = block.ob.transform.position.y;
            position[2] = block.ob.transform.position.z;
            nameSprite = block.ob.GetComponent<SpriteRenderer>().sprite.name;
            isNull = false;
        }
    }
    public override string ToString()
    {
        return "NULL: "+isNull+", x: "+x+", y: "+y+", pos( x:  "+position[0]+" , y: "+position[1]+", z: "+position[2]+")"+", sprite: "+nameSprite;
    }
}
