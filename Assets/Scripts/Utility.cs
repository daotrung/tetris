﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility
{
    namespace DesignPatterns {
       //Singleton
        #region Singleton
        public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
        {
            public static T instance = null;
            private static bool shuttingDown = false;

            public static T Instance
            {
                get
                {
                    if (instance == null && !shuttingDown && Application.isPlaying)
                    {
                        instance = FindObjectOfType(typeof(T)) as T;
                        if (instance == null)
                        {
                            Debug.Log("No instance of" + typeof(T).ToString() + ", a temporary instance created.");
                            instance = new GameObject("Temp Instance of" + typeof(T).ToString(), typeof(T)).GetComponent<T>();
                        }
                    }
                    return instance;
                }

            }

            protected virtual void Awake()
            {
                if (instance == null)
                {
                    instance = this as T;
                } else if (instance != this)
                {
                    Debug.LogError("Another instance of" + GetType() + "is aleary exist ! Destroy seft...");
                    DestroyImmediate(gameObject);
                    return;
                }
            }

            protected virtual void OnDestroy()
            {
                if (this == instance)
                {
                    instance = null;
                }
            }

            protected virtual void OnApplicationQuit()
            {
                instance = null;
                shuttingDown = true;
            }
        }
        #endregion
    }

    namespace Optimize
    {
        #region ObjectPooling
        [System.Serializable]
        public class ObjectPooler : MonoBehaviour
        {

            // Start is called before the first frame update
            void Start()
            {

            }

            // Update is called once per frame
            void Update()
            {

            }
        }
        #endregion
    }
}
