﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class CameraShakeByDirection : MonoBehaviour
{
    public Camera camera;
    public float magnitude;
    public float duration;
    public Vector3 direction;
    [SerializeField]private bool isRunning;
    // Start is called before the first frame update
    void Start()
    {
        if(camera == null)
        {
            camera = GetComponent<Camera>();
        }
        if(camera == null)
        {
            Debug.LogWarning("CAN'T FIND THE CAMERA FOR CAMERA SHAKING");
            return;
        }
      
    } 
    public void shake(Vector3 dir)
    {
        if (isRunning) return;
        direction = dir;
        if(camera.orthographic)
        {
            StartCoroutine(ShakeOrtho());
        }
        else
        {
            StartCoroutine(ShakePerspective());
        }
    }
    IEnumerator ShakePerspective()
     {
        yield return null;
    }
    //IEnumerator ShakeOrtho()
    //{
    //    float elapsed = 0.0f;
    //    Vector3 originalPos =Camera.main.transform.position;
    //    while(elapsed < duration)
    //    {
    //        elapsed += Time.deltaTime;
    //        float percentCompelete = elapsed / duration;
    //        float damper = 1.0f - Mathf.Clamp(4.0f * percentCompelete -3.0f , 0.0f, 1.0f);
    //        //random


    //        float x = Random.value * 2.0f - 1.0f;
    //        float y = Random.value * 2.0f - 1.0f;
    //        x *= magnitude * damper * direction.x ;
    //        y *= magnitude * damper * direction.y;

    //        Camera.main.transform.position = new Vector3(x, y,0)+ originalPos;
    //        yield return null;
    //    }
    //    Camera.main.transform.position = originalPos;
    //    yield return null;
    //}    

    //magnitude = 3 ,duration = 0.8f
    IEnumerator ShakeOrtho()
    {
        isRunning = true;
        float elapsed = 0.0f;
        Vector3 originalPos = Camera.main.transform.localPosition;
        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            float percentCompelete = elapsed / duration;
            float x = Mathf.PerlinNoise(0,percentCompelete * magnitude) * 2.0f - 1.0f;
            float y = Mathf.PerlinNoise(0,percentCompelete * magnitude) * 2.0f - 1.0f;
            float z = Mathf.PerlinNoise(0,percentCompelete * magnitude) * 2.0f -1.0f;
            x   *= direction.x;
            y *=    direction.y;
            z *=  direction.z;
            Camera.main.transform.localPosition = new Vector3(x, y, z) + originalPos;
            yield return null;
        }
        Camera.main.transform.localPosition = originalPos;
        isRunning = false;
        yield return null;
       
    }
    //magnitude 1 duration - 0.1f
    //IEnumerator ShakeOrtho()
    //{
    //    isRunning = true;
    //    float elapsed = 0.0f;
    //    Vector3 originalPos = Camera.main.transform.localPosition;
    //    while (elapsed < duration)
    //    {
    //        elapsed += Time.deltaTime;
    //        float percentCompelete = elapsed / duration;
    //        float x = Mathf.PerlinNoise(0, percentCompelete ) * 2.0f - 1.0f;
    //        float y = Mathf.PerlinNoise(0, percentCompelete ) * 2.0f - 1.0f;
    //        float z = Mathf.PerlinNoise(0, percentCompelete ) * 2.0f - 1.0f;
    //        x *= magnitude * direction.x;
    //        y *= magnitude *  direction.y;
    //        z *= magnitude *direction.z;
    //        Camera.main.transform.localPosition = new Vector3(x, y, z) + originalPos;
    //        yield return null;
    //    }
    //    Camera.main.transform.localPosition = originalPos;
    //    isRunning = false;
    //    yield return null;

    //}
}
